
import kotlin.math.PI
import kotlin.math.roundToInt

//bit is (2cm)^3
class Bit(var x: Int, var y: Int, var z: Int, val c: Color) {
    val plane = Plane(x.toDouble(), y.toDouble(), z.toDouble())

    fun rotate(num:Int, foo:(Plane, Double) -> Unit){
        foo(plane, num * PI / 2)
        x = plane.x.roundToInt()
        y = plane.y.roundToInt()
        z = plane.z.roundToInt()
    }

    fun getPosition(height: Int): String {
        return when (height) {
            x -> "PX"
            y -> "PY"
            z -> "PZ"
            -x -> "MX"
            -y -> "MY"
            -z -> "MZ"
            else -> {
                throw Error("fuck")
            }
        }
    }

    override fun toString(): String {
        return "x: ${x}, y: ${y}, z: ${z}, color: ${c}"
    }
}