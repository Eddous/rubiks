import kotlin.random.Random.Default.nextInt

fun main(args: Array<String>) {
    tests()

    val not = Rubiks(2)
    val yes = Rubiks(2)
    for (i in 0..10000){
        yes.randomMove()
    }
    var noCorr = 0
    var yesCorr = 0
    var moves = 0
    while (true){
        not.randomMove()
        yes.randomMove()
        if(not.isCorrect()){
            noCorr++
            println("NOT "+noCorr.toString() + " " +moves.toString())
        }
        if(yes.isCorrect()){
            yesCorr++
            println("YES "+yesCorr.toString() + " " +moves.toString())
            for (i in 0..10000){
                yes.randomMove()
            }
        }
        moves++
    }
}

fun tests() {
    rubAfterInit()
    bitRotationConsistency()
}

fun myAssert(a: Boolean) {
    if (!a) {
        throw Error(":0")
    }
}

fun rubAfterInit() {
    myAssert(Rubiks(1).bits.size == 6)
    myAssert(Rubiks(2).bits.size == 24)
    myAssert(Rubiks(3).bits.size == 54)
    myAssert(Rubiks(1).isCorrect())
    myAssert(Rubiks(2).isCorrect())
    myAssert(Rubiks(3).isCorrect())
}

fun bitRotationConsistency() {
    val bits = ArrayList<Bit>()
    for (i in (0..100)) {
        bits.add(Bit(nextInt(-1000, 1000), nextInt(-1000, 1000), nextInt(-1000, 1000), Color.W))
    }
    val foos = ArrayList<(Bit) -> Unit>()
    foos.addAll(generateLamdas { b: Bit, i: Int -> b.rotate(i,{p:Plane, d:Double -> p.RX(d)})})
    foos.addAll(generateLamdas { b: Bit, i: Int ->  b.rotate(i,{p:Plane, d:Double -> p.RY(d)})})
    foos.addAll(generateLamdas { b: Bit, i: Int ->  b.rotate(i,{p:Plane, d:Double -> p.RZ(d)})})
    for (foo in foos) {
        for (b in bits) {
            bitRotationConsistency(b, foo)
        }
    }
}

fun findAllTest() {
    throw Error("not supported")
    val r = Rubiks(2)
}

fun bitRotationConsistency(b: Bit, foo: (b: Bit) -> Unit) { //foo take do some work and return same obj
    val x = b.x
    val y = b.y
    val z = b.z
    foo(b)
    myAssert(b.x == x)
    myAssert(b.y == y)
    myAssert(b.z == z)
}

fun generateLamdas(foo: (b: Bit, i: Int) -> Unit): ArrayList<(Bit) -> Unit> {
    val list = ArrayList<(Bit) -> Unit>()
    list.add { b: Bit ->
        foo(b, 1)
        foo(b, 1)
        foo(b, 1)
        foo(b, 1)
    }
    list.add { b: Bit ->
        foo(b, -1)
        foo(b, -1)
        foo(b, -1)
        foo(b, -1)
    }
    list.add { b: Bit ->
        foo(b, 1)
        foo(b, -1)
    }
    return list
}

