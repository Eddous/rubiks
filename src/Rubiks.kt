
import java.util.*
import kotlin.collections.ArrayList
import kotlin.random.Random

class Rubiks(val size: Int) {
    val bits = ArrayList<Bit>()

    //note height = size
    init {
        for ((a, b) in generateFace(size)) {
            bits.add(Bit(a, b, size, Color.W))
            bits.add(Bit(a, b, -size, Color.Y))
            bits.add(Bit(a, size, b, Color.O))
            bits.add(Bit(a, -size, b, Color.R))
            bits.add(Bit(size, a, b, Color.B))
            bits.add(Bit(-size, a, b, Color.G))
        }
    }

    fun isCorrect(): Boolean {
        var map = TreeMap<String, Color>()
        for (bit in bits) {
            val pos = bit.getPosition(size)
            if (!map.containsKey(pos)) {
                map[pos] = bit.c
            }
            if (map[pos] != bit.c) {
                return false
            }
        }
        return true
    }

    fun randomMove() {
        val along = Random.nextInt(3)
        val len = 2*Random.nextInt(2) - 1
        val bit = bits.get(Random.nextInt(bits.size))
        if(along == 0){
             rotate(bit, len, {b:Bit -> b.x},{p:Plane, d:Double -> p.RX(d)})
        }
        if(along == 1){
            rotate(bit, len, {b:Bit -> b.y},{p:Plane, d:Double -> p.RY(d)})
        }
        if(along == 2){
            rotate(bit, len, {b:Bit -> b.z},{p:Plane, d:Double -> p.RZ(d)})
        }
    }

    fun rotate(bit:Bit, num:Int, getter: (Bit) -> Int, rotator:(Plane, Double) -> Unit){
        for (b in findAllOnLevelAs(bit,getter)){
            b.rotate(num,rotator)
        }
    }

    fun findAllOnLevelAs(bit: Bit, getter: (Bit) -> Int): ArrayList<Bit> { //getter gets height
        val list = ArrayList<Bit>()
        val level = getter(bit)
        for (b in bits) {
            val current = getter(b)
            if (current == level || current == level - 1 || current == level + 1) {
                list.add(b)
            }
        }
        return list;
    }



    override fun toString(): String { //it will be hardcore hard-code
        val top = getFace({a -> a.z == size},{a -> -a.y},{a -> a.x})
        val front = getFace({a -> a.y == -size},{a -> -a.z},{a -> a.x})
        val right = getFace({a -> a.x == size},{a -> -a.z},{a -> a.y})
        val raster = Array(2*size){Array(2*size){" "} }

        for (b in top){
            println(b)
        }
        copyYourselfIn(size, top, raster, 0,0 )
        copyYourselfIn(size, front, raster, 0,size)
        copyYourselfIn(size, right, raster, size,size )
        /*for (line in 0 until size) {
            shiftRow(line, size,size-1-line,raster)
            shiftColumn(size+size-1-line, size,size-1-line,raster)
        }*/
        val new = makeSpaces(raster)
        val str = StringBuilder()
        for (y in new[0].indices){
            for (x in new.indices){
                str.append(new[x][y])
            }
            str.append('\n')
        }
        return str.toString()
    }

    fun getFace(isOnFace: (Bit) -> Boolean, first:(Bit)->Int, second:(Bit)->Int): List<Bit> {
        var list = ArrayList<Bit>()
        for (bit in bits) {
            if (isOnFace(bit)) {
                list.add(bit)
            }
        }
        val rule = {a: Bit,b:Bit ->
            when {
                first(a)>first(b) -> 1
                first(a)<first(b) -> -1
                second(a)>second(b) -> 1
                second(a)<second(b) -> -1
                else -> 0
            }
        }
        val tmp = list.sortedWith(Comparator<Bit>(rule))
        return tmp
    }

    companion object {
        fun generateFace(size: Int): ArrayList<Pair<Int, Int>> {
            var list = ArrayList<Pair<Int, Int>>()
            val to = size - 1
            val from = -to
            for (x in from..to step 2) {
                for (y in from..to step 2) {
                    list.add(Pair(x, y))
                }
            }
            return list
        }

        fun copyYourselfIn(size:Int, list: List<Bit>, raster:Array<Array<String>>, startX:Int, startY:Int){ //array [x] [y]
            var x = 0
            var y = 0
            for (bit in list){
                if (x == size){
                    y++
                    x = 0
                }
                raster[startX+x][startY+y] = bit.c.name
                x++
            }
        }

        fun makeSpaces(raster:Array<Array<String>>):Array<Array<String>>{
            val new = Array(raster.size*2) { Array(raster[0].size) { " " } }
            for (x in (raster.indices)){
                for (y in (raster[0].indices)){
                    new[2*x][y] = raster[x][y]
                }
            }
            return new
        }

        fun shiftRow (y: Int,toShift: Int, shift:Int, raster:Array<Array<String>>){ //shift to right
            for (x in (toShift-1 downTo 0)) {
                raster[x+shift][y] = raster[x][y]
            }
            for (x in 0 until shift){
                raster[x][y] = " "
            }
        }

        fun shiftColumn(x: Int, toShift: Int, shift:Int, raster:Array<Array<String>>){ //shift up
            val size = raster[0].size
            for (y in (size-toShift-shift until size-toShift)) {
                raster[x][y] = raster[x][y+shift]
            }
            for (y in 0 until shift){
                raster[x][y+size-shift] = " "
            }
        }
    }
}
