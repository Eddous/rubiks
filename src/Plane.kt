import kotlin.math.cos
import kotlin.math.sin

open class Plane(
    var x: Double,
    var y: Double,
    var z: Double
) {
    fun RX(degree: Double) {
        var tup = rotate(y, z, degree)
        y = tup.first
        z = tup.second
    }

    fun RY(degree: Double) {
        var tup = rotate(z, x, degree)
        z = tup.first
        x = tup.second
    }

    fun RZ(degree: Double) {
        var tup = rotate(x, y, degree)
        x = tup.first
        y = tup.second
    }

    companion object {
        fun rotate(x: Double, y: Double, rads: Double): Pair<Double, Double> {
            return Pair(x * cos(rads) - y * sin(rads), x * sin(rads) + y * cos(rads))
        }
    }
}